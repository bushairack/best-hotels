"use strict";
const TOPTABS = document.querySelectorAll(".topnav button");
// console.log(TOPTABS);

const PAGES = document.querySelectorAll("article");
//  console.log(PAGES);

let cities = ["Munich", "Nürnberg", "Berlin", "Hamburg", "Frankfurt"];
let hotelList = ["Eurostars", "HolidayInn", "Maritim", "DreiLöwen"];
let roomsList = ["single_standerd", "single_superior", "doubleroom_standerd", "doubleroom_superior"];
let NoOfAdults = 2, NoOfKids = 0, NoOfRooms = 1, NoOfDays = 1;
let hideData = function () {
    for (let i = 0; i < PAGES.length; i++) {
        PAGES[i].hidden = true;
    }
};
hideData();
for (let i = 0; i < PAGES.length; i++) {
    PAGES[0].hidden = false;
    if (i == 0) {
        selectedCities(i);

    }
    if (i == 1) {
        careerPage(i);
    }

    TOPTABS[i].addEventListener("click", function () {
        hideData();
        activTab(i);

    });
}
function activTab(currentTab) {
    PAGES[currentTab].hidden = false;

}
function selectedCities(i) {
    let search = document.querySelector("div .details");
    console.log(search);
    search.hidden = true;
  
    

    let ourCitiesHeading = document.createElement("h2");
    ourCitiesHeading.textContent = "Select the city to book the Hotel room";
    ourCitiesHeading.className = "ourCitiesHeading";
    PAGES[i].append(ourCitiesHeading);
    for (let j = 0; j < cities.length; j++) {
        let ourCities = document.createElement("button");
        ourCities.textContent = cities[j];
        ourCities.className = cities[j];
        pageOpen(ourCities);
        PAGES[i].append(ourCities);

    }
    function pageOpen(ourCity) {
        // for(let k=0; k<cities.length; k++) {
        ourCity.addEventListener("click", function () {
            hotelsOftheCity();
        });
        function hotelsOftheCity() {
            hideData();
            let hotels = document.querySelector("#hotels");
            let info = document.createElement("p");
            info.textContent = "You can simply click the Hotel names for selecting the available rooms";
            info.className = "info";
            let xhr = new XMLHttpRequest();
            let availableRooms = document.createElement("button");
            hotels.append(info);
            xhr.onload = function () {
                if (xhr.status != 200) return;
                let xml = xhr.responseXML;
                let hotelNames = xml.querySelectorAll("hotel");
                console.log(hotelNames);
               
                for (let j = 0; j < hotelNames.length; j++) {
                    console.log(hotelNames.length);
                    
                    availableRooms[j] = document.createElement("button");
                    availableRooms[j].className = hotelList[j];
                    availableRooms[j].innerHTML =
                        hotelNames[j].querySelector("name").textContent;
                    
                    console.log(availableRooms);

                    hotels.append(availableRooms[j]);
                    availableRooms[j].addEventListener("click", function () {
                        // careerPage(hotels);
                        roomsdetail(hotels, j);

                    });

                }
            }
            xhr.open("GET", "hotels.xml");
            xhr.responseType = "document";
            xhr.send();
        }
        function roomsdetail(hotels, j) {

            console.log(hotels, "hotels");
            hotels.hidden = true;
            let Rooms = document.querySelector("#Rooms");
            let xhr = new XMLHttpRequest();
            let rooms = document.createElement("button");
            let roomsType;
            xhr.onload = function () {
                if (xhr.status != 200) return;
                let xml = xhr.responseXML;
                if (j == 0) {
                    roomsType = xml.querySelectorAll("EurostarsRoom");
                } else if (j == 1) {
                    roomsType = xml.querySelectorAll("HolidayInnRoom");
                } else if (j == 2) {
                    roomsType = xml.querySelectorAll("HotelMaritimRoom");
                } else {
                    roomsType = xml.querySelectorAll("HotelDreiLöwenRoom");
                }
                console.log(roomsType);

                typesOfRooms(roomsType);
            }
            xhr.open("GET", "hotels.xml");
            xhr.responseType = "document";
            xhr.send();

            function typesOfRooms(roomsType) {
                for (let j = 0; j < roomsType.length; j++) {
                    console.log(roomsType.length);
                    rooms[j] = document.createElement("button");
                    rooms[j].className = roomsList[j];

                    rooms[j].innerHTML =
                        roomsType[j].querySelector("name").textContent +
                        "<br> Price: " + roomsType[j].querySelector("price").textContent + "€" +
                        "<br> Breakfast - " + roomsType[j].querySelector("breakfast").textContent +
                        "<br> Ratings: " + roomsType[j].querySelector("rating").textContent +
                        "<br> Available No. of Rooms: " + roomsType[j].querySelector("available").textContent;
                    // console.log(rooms);

                    Rooms.append(rooms[j]);
                    let bookButton = document.createElement("button");
                    bookButton.textContent = "Book Now";
                    bookButton.className = "bookingButton";
                    Rooms.append(bookButton);
                    bookButton.addEventListener("click", function () {
                        let roomPrice = roomsType[j].querySelector("price").textContent;
                        let roomsAvailablity = roomsType[j].querySelector("available").textContent;
                        console.log("room price is", roomPrice);
                        bookTo(Rooms, roomPrice, roomsAvailablity);

                    });
                }
            }

        }
        function bookTo(Rooms, roomPrice, roomsAvailablity) {
            let roomsAvailablityCheck = roomsAvailablity;
            console.log(roomsAvailablityCheck);
            let count = 0;
            let today = new Date();
            let dd = today.getDate();

            let mm = today.getMonth() + 1;
            let bookLimitDay = today.getMonth() + 3;
            let yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }
            // today = dd+'-'+mm+'-'+yyyy;
            today = yyyy + '-' + mm + '-' + dd;
            // console.log(today);
            dd = dd + 1;
            let nextday = yyyy + '-' + mm + '-' + dd;
            let bookingLimit = yyyy + '-' + bookLimitDay + '-' + dd;
            //  console.log(bookingLimit, "limit");


            Rooms.hidden = true;
            let checkIn = document.querySelectorAll('input[type="date"]');

            // let dates = ["Check-In", "Check-Out"];
            let inDate = document.createElement("p");
            inDate.textContent = "Check-In";
            inDate.className = "inDate";

            let outDate = document.createElement("p");
            outDate.textContent = "Check-Out";
            outDate.className = "outDate";

            let roomBooking = document.querySelector("#Booking");
            for (let j = 0; j < checkIn.length; j++) {
                // console.log(checkIn.length, "length");
                if (j == 0) checkIn[j].value = today;
                else checkIn[j].value = nextday;

                checkIn[j].min = today;
                checkIn[j].max = bookingLimit;

                checkIn[j].className = "dateIn";
                // console.log(checkIn[j].value);
                if (count == 0) {
                    roomBooking.append(inDate, checkIn[0], outDate, checkIn[1]);
                }
            }
            let dateIn, dateOut;
            for (let k = 0; k < checkIn.length; k++) {
                checkIn[k].addEventListener("change", function () {

                    if (k == 0) dateIn = checkIn[k].value;
                    if (k == 1) dateOut = checkIn[k].value;
                    console.log(dateIn, "check in");
                    console.log(dateOut, "check out");

                    // NoOfDays = NoOfDays*1;

                    // console.log(NoOfDays, "days")
                    if (dateIn > dateOut) {
                        count = count + 1;
                        alert("Please enter your check in and check out dates correctly");

                        // bookTo(Rooms);    
                    }

                    const date1 = new Date(dateIn);
                    const date2 = new Date(dateOut);
                    const diffTime = Math.abs(date2 - date1);
                    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    console.log(diffDays + " days");
                    NoOfDays = diffDays * 1;
                    console.log(NoOfDays);


                });
            }

            let adultSelect = document.querySelector("#no-of-persons");
            console.log(adultSelect.selectedIndex);
            adultSelect.className = "no-of-persons";
            roomBooking.append(adultSelect);
            adultSelect.addEventListener("change", function () {
                // console.log(adultSelect.value);
                adults(adultSelect.value);
            });

            function adults(noOfAdults) {
                if (noOfAdults > 6) {
                    noOfAdults = prompt("How many number of adults?");
                    console.log(noOfAdults);
                    NoOfAdults = noOfAdults;
                } else NoOfAdults = noOfAdults;
                console.log(noOfAdults);

            }



            let childrenSelect = document.querySelector("#no-of-children");
            console.log(childrenSelect.selectedIndex);
            childrenSelect.className = "no-of-children";
            roomBooking.append(childrenSelect);
            childrenSelect.addEventListener("change", function () {
                // console.log(adultSelect.value);
                children(childrenSelect.value);
            });

            function children(noOfKids) {
                if (noOfKids > 6) {
                    noOfKids = prompt("How many number of children with you?");
                    console.log(noOfKids);
                    NoOfKids = noOfKids;
                } else
                    NoOfKids = noOfKids;
                console.log(noOfKids);

            }


            let roomCount = document.querySelector("#no-of-rooms");
            console.log(roomCount.selectedIndex);
            roomCount.className = "no-of-rooms";
            roomBooking.append(roomCount);
            roomCount.addEventListener("change", function () {
                // console.log(adultSelect.value);
                requiredRooms(roomCount.value);
            });

            function requiredRooms(noOfRooms) {
                if (noOfRooms > 6) {
                    noOfRooms = prompt("How many number of children with you?");
                    console.log(noOfRooms);
                    NoOfRooms = noOfRooms;
                } else NoOfRooms = noOfRooms;


            }
            let clickButton = document.createElement("button");
            clickButton.textContent = "Book";
            clickButton.className = "clickButton";
            roomBooking.append(clickButton);
            clickButton.addEventListener("click", function () {
                let roomsCount;
                console.log("no of rooms", NoOfRooms);
                console.log("rooms available", roomsAvailablityCheck);
                if ((NoOfRooms * 1) < (roomsAvailablityCheck * 1)) {
                    roomsCount = roomsAvailablityCheck - NoOfRooms;
                    let amount = NoOfRooms * roomPrice * NoOfDays;
                    // console.log(NoOfRooms, roomPrice, amount, NoOfDays);
                    Payment(amount, roomBooking, roomsCount);
                }
                else {
                    alert("Sorry! " + NoOfRooms + " rooms are currently not available");

                }
            });

        }
        function Payment(amount, roomBooking, roomsCount) {
            let confirm = document.querySelector("#paymentStep");
            roomBooking.hidden = true;
            let confirmStatement = document.createElement("p");
            confirmStatement.textContent = "Please confirm your booking details";
            confirmStatement.className = "statementp";

            let roomStatement = document.createElement("p");
            roomStatement.textContent = "Number of rooms : " + NoOfRooms;
            roomStatement.className = "statement";

            let adultStatement = document.createElement("p");
            adultStatement.textContent = "Number of adults : " + NoOfAdults;
            adultStatement.className = "statement";

            let kidsStatement = document.createElement("p");
            kidsStatement.textContent = "Number of children : " + NoOfKids;
            kidsStatement.className = "statement";

            let dayStatement = document.createElement("p");
            dayStatement.textContent = "Number of days : " + NoOfDays;
            dayStatement.className = "statement";

            let amountStatement = document.createElement("p");
            amountStatement.textContent = "Total amount : " + amount + " €";
            amountStatement.className = "amountStatement";

            confirm.append(confirmStatement,roomStatement , adultStatement, kidsStatement, dayStatement, amountStatement);
            let block = document.createElement("button");
            block.textContent = "Confirm";
            block.className = "confirmation";
            confirm.append(block);
            console.log(confirm);
            block.addEventListener("click", function () {

                blockedRooms(roomsCount, confirm);
            });
        }
        function blockedRooms(roomsCount, confirm) {
            console.log("You booking is completed", roomsCount);
            let userData = document.createElement("p");
            let endPage = document.querySelector("#end");


            confirm.hidden = true;
            userData.textContent = "For booking the rooms, please provide your details";
            userData.className = "userData";
            
            let userInfo, userLastName, userEmail, usercontact;
            let fstName = document.createElement("p");
            fstName.textContent = "First name:"
            fstName.className = "fst";
            let firstName = document.createElement("input");
            firstName.className = "First_name";
            firstName.addEventListener("change", function () {
                userInfo = (firstName.value).trim();
                console.log(userInfo);
            });

            let lstName = document.createElement("p");
            lstName.textContent = "Last name:";
            lstName.className = "lst";
            let lastName = document.createElement("input");
            lastName.className = "Last_name";
            lastName.addEventListener("change", function () {
                userLastName = (lastName.value).trim();
                console.log(userLastName);
            });

            let email = document.createElement("p");
            email.textContent = "E-mail id:";
            email.className = "email";
            let emailId = document.createElement("input");
            emailId.className = "e_name";
            emailId.addEventListener("change", function () {
                userEmail = (emailId.value).trim();
                let index = userEmail.indexOf("@");
                console.log(index);
                if (index <= 0) {
                    alert("Please provide valid E-mail id");
                    userEmail = "";
                }
                console.log(userEmail);
            });

            let mob = document.createElement("p");
            mob.textContent = "Contact number:";
            mob.className = "mob";
            let mobNo = document.createElement("input");
            mobNo.className = "mobNo";
            mobNo.addEventListener("change", function () {
                usercontact = (mobNo.value).trim();
                if (isNaN(usercontact)) {
                    alert("Please provide a valid number");
                    usercontact = 0;
                }
                console.log(usercontact);
            });
            endPage.append( userData);
            endPage.append(fstName, firstName);
            endPage.append(lstName, lastName);
            endPage.append(email, emailId);
            endPage.append(mob, mobNo);

            let submitButton = document.createElement('button');
            submitButton.textContent = "Submit";
            submitButton.className = "submit";

            submitButton.addEventListener("click", function () {
                if (userInfo || userLastName || userEmail || usercontact) {
                    submitButton.disabled = false;

                    let user = {
                        firstName:  userInfo,
                        lastName:   userLastName,
                        Email:      userEmail,
                        mobile:     usercontact
                    };

                    localStorage.setItem("userData", JSON.stringify(user));
                    console.log(JSON.parse(localStorage.getItem("userData")));

                    document.cookie = "user=" + userInfo + ' ' + userLastName + ', Email Id :-' + userEmail + ', Mob:- ' + usercontact +'; path=/';
                    function readCookie(name) {
                        let nameEQ = name + '=';
                        let ca = document.cookie.split(';');
                        for (let i = 0; i < ca.length; i++) {
                            let c = ca[i];
                            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                        }
                        return null;
                    }
                    console.log(readCookie("user"));
                    clickFunction();
                    endClick(endPage);

                } else {
                    alert("Please fill the form ");
                    submitButton.disabled = false;
                }
            });
            function clickFunction() {
                let counter = 1; 
                let storage = sessionStorage.getItem("klicks");
                if(storage) counter = Number(storage) + 1;
                sessionStorage.setItem("klicks",counter);
                endPage.textContent = "First Name :- "+userInfo + "\n LastName :- " +userLastName + "\nEmail ID :- "  + userEmail + " Mob :- " +usercontact ;
            }
            endPage.append(submitButton);
            

        }
        function endClick(endPage){
            let thanksPage = document.querySelector("#thanks");
            endPage.hidden = true;
            
            thanksPage.insertAdjacentHTML("afterend", ("<p id='userData'>"+"You are successfully booked your rooms" + "</p>" +"<p id='call'>" + "<br> Thank you for using our site. " + "</p>" + "<hr>" + "<p id='appContact'>" + "<br>A member of our team will shortly get back to you through e-mail and confirm your details." + "<br> If you have any queries please feel to contact us at below details " + "</p>" + "<hr>" + "<p id='userData'>" + "<br>HotelFinder-<i>Germany</i>.com" + "<br>E-mail: hotelbooking_germany@htc.com <br>Tel: 0151 245837 <br>Germany " + "</p>"));                         
        }

    }
}
function careerPage(i) {
    hideData();
    let search = document.querySelector("div .details");
    console.log(search);
    search.hidden = true;
   /*  let endPage = document.querySelector("#end");
    endPage.hidden = true; */
    
    
    // let pageData = document.querySelector(".second");
    // console.log(pageData);
    // let details = document.createElement("p");
    // details.textContent = "this is the booking process page";
    // pageData.append(details);

}

